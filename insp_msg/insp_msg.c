#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/XKBlib.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_FACTS 4000
#define LINE_LENGTH 200

#define TRUE 1
#define FALSE 0

int numFacts;
void getFacts(void);

char facts[MAX_FACTS][LINE_LENGTH];

static int xkb_event_base = 0;
static int xkb_error_base = 0;

int main(void) {
  Display *disp;
  int opcode;
  int maj = XkbMajorVersion;
  int min = XkbMinorVersion;

  /* Open Display */
  if ( !(disp = XOpenDisplay(NULL))) {
    fprintf(stderr, "Can't open display: CHECK DISPLAY VARIABLE\n");
    exit(1);
  }

  if (!XkbLibraryVersion(&maj, &min)) {
    fprintf(stderr, "Couldn't get Xkb library version\n");
    exit(1);
  }
  
  if (!XkbQueryExtension(disp, &opcode, &xkb_event_base, &xkb_error_base, &maj, &min)) {
    fprintf(stderr, "XkbQueryExtension error\n");
    exit(1);
  }

  if (!XkbSelectEvents(disp, XkbUseCoreKbd, XkbIndicatorStateNotifyMask, 
                       XkbIndicatorStateNotifyMask)) {
    fprintf(stderr, "XkbSelectEvents\n");
    exit(1);
  }

// fill facts array
  getFacts();
  
//let t seed the lcg
  time_t t;
  srand((unsigned) time(&t));
  int last = 0;
  int next = last; 
  while (TRUE) {
//for the purposes of not showing the same fact two times in a row
    while (next == last){
      next = rand() % numFacts;
    }

//print out fact to the stdout
    fflush(stdout);
    printf("%s", facts[next]);
    last = next;
//sleep and repeat
    sleep (10);
  }
  return 0;
}

void getFacts(){
  //read facts.txt file, and store facts into facts array
  FILE *f = fopen("/home/andrew/.xmonad/facts.txt", "r");
  if (f == NULL){
    printf("Unable to open file");
    exit(1);
  }

  char line[1000]; //line buffer
  while (fgets(line, 1000, f) && numFacts < MAX_FACTS){
    if (strlen(line) <= LINE_LENGTH){
      strncpy(facts[numFacts++], line, LINE_LENGTH);
    }
  }
  fclose(f);
}
