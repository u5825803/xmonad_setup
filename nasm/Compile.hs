import Control.Monad.State.Strict
import Parser
import Text.Printf
import System.Environment (getArgs)

main :: IO ()
main = do
  [file] <- getArgs
  Right parsed <- runParser parseProgram () file <$> readFile file
  let prog = evalState (compile parsed) 0
  writeFile "out.asm" (header ++ prog ++ tailer)
  
compile :: [Token] -> State Int String
compile lst = do
  count <- get
  case lst of
    [] -> return []
    t:ts -> case t of
      Increment -> liftM ("\t;increment\n\tadd  byte [r12],  1\n"++) (compile ts)
      Decrement -> liftM ("\t;decrement\n\tsub  byte [r12],  1\n"++) (compile ts)
      PtrInc -> liftM ("\t;ptr inc\n\tadd  r12,  1\n"++) (compile ts)
      PtrDec -> liftM ("\t;ptr dec\n\tsub  r12,  1\n"++) (compile ts)
      PutChar -> liftM ("\t;put char\n\tmov  rsi,  r12\n\tsyscall\n"++) (compile ts)
      GetChar -> liftM ("\t;get char\n\txor  rax,  rax\n\txor  rdi,  rdi\n\tmov  rsi,  r12\n\tsyscall\n\tmov  rax,  1\n\tmov  rdi,  1\n"++) (compile ts)
      Loop prog -> do
        modify succ
        inner <- compile prog
        liftM ((printf "loopBlock%d:\n\tcmp  byte [r12],  0\n\tje contBlock%d\n%s\tjmp  loopBlock%d\ncontBlock%d:\n" count count inner count count)++) (compile ts)

header :: String
header = "\t;header\n\tglobal _start\n\n\tsection .text\n_start:\n\tmov  r12,  runner\n\n\tmov  rax,  1\n\tmov  rdi,  1\n\tmov  rdx,  1\n"

tailer :: String
tailer = "\t;tailer\n\t;exit\n\tmov  eax,  60\n\txor  rdi,  rdi\n\tsyscall\n\n\tsection .data\nrunner:\n\tresb  30000\n"
