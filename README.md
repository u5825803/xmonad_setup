Linux Mint / Ubuntu Install

--in the terminal, enter into xmonad_setup

--Install x11 libs from the apt repository
$ sudo apt-get install libx11-dev

--check if the libxinerama-dev package is installed on your computer, this helps with multiple monitor setup, if not
$ sudo apt-get install libxinerama-dev

--install the useful tools required (from the apt repository)
$ sudo apt-get install haskell-platform
$ sudo apt-get install suckless-tools xscreensaver stalonetray feh python-pip xfce4-power-manager
$ //sudo apt-get install xmobar
$ sudo pip install udiskie
$ sudo apt-get install libxrandr-dev

--cabal install xmonad and some helpful libaries
$ cabal install x11
$ cabal install xmonad-contrib --flags="-use_xft"
$ cabal install xmonad
$ cabal install xmobar --may not work

--copy setup and config files into ~/ directory
--copy .stalonetrayrc, .xmobardownrc, .xmobaruprc .xmonad/ to ~/ folder

--make helper programs using gcc

--enter insp_msg/ folder in the terminal
$ make
$ mv insp_msg ~/.xmonad/
$ mv facts.txt ~/.xmonad/

--enter ledmon/ fold in termial
$ make
$ mv ledmon ~/.xmonad/

--add ~/.cabal/bin to $PATH variable, or symlink xmonad into /usr/bin
--sudo visudo, append: andrew ALL=NOPASSWD: /usr/sbin/pm-suspend
--to pm-suspend without password

--copy backgrounds into $HOME/Pictures/
--copy facts.txt into ~/.xmonad

--test xmonad setup in terminal
$ xmonad --recompile

$ xmonad //should test if all files are in place

--install extras
$ sudo apt-get install curl



Arch Linux install

--Install xorg as per instructions (very roughly)
--install alsa-utils
--install graphics card drivers

--install xmonad & xmonad-contrib
--create ~/.xmonad/xmonad.hs and in that put (a stub)
	import XMonad
	
	main :: IO()
	main = xmonad defaultConfig

--create ~/.xinitrc
--put "exec xmonad" in ~/.xinitrc

--install xdm with xdm-archlinux and enable xdm-archlinux.service 
--reboot and git clone xmonad-setup repo (with this file in it)
--install xmobar, stalonetray, python-pip -> udiskie, xfce4-power-manager, rxvt-unicode (terminal emulator), chromium, gnome-system-monitor, ... (whatever you want)
--copy over files (usual drill) and compile 
$ xmonad --recompile
and test 
$ xmonad


