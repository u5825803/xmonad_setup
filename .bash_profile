#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

export PATH="~/.stack/snapshots/x86_64-linux/lts-3.17/7.10.2/bin/:~/.cabal/bin:$PATH"
export _JAVA_AWT_WM_NONREPARENTING=1
