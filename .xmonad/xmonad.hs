{-# LANGUAGE NoImplicitPrelude #-}

import Control.Monad (Monad (..), unless, mapM_, void)
import Data.Array (Array, array, elems, (!))
import Data.Function (($), (.), flip)
import Data.Int (Int)
import Data.List ((++))
import System.Directory (getHomeDirectory)
import System.IO (IO, FilePath, hPutStrLn)
import System.Posix.Files (fileExist)
import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops (ewmh)
import XMonad.Hooks.ManageDocks (avoidStruts, manageDocks)
import XMonad.StackSet (greedyView)
import XMonad.Util.EZConfig (additionalKeys, removeKeys)
import XMonad.Util.Run (spawnPipe)
import XMonad.Actions.Volume

-- Why use ()? Lets make our own unit datatype
data Unit = One

pictures :: Array Int FilePath
pictures = array (1, 9) [
  (1, "haskell1.jpg"),
  (2, "haskell2.jpg"),
  (3, "haskell3.jpg"),
  (4, "haskell4.jpg"),
  (5, "haskell5.jpg"),
  (6, "haskell7.jpg"),
  (7, "pink-floyd-back-catalogue.jpg"),
  (8, "tonights_the_night.jpg"),
  (9, "darth.jpg")]

background_folder :: FilePath
background_folder = "/Pictures/backgrounds/"

checkFile :: FilePath -> IO()
checkFile file = fileExist file >>= flip unless (fail ("Error: the file "
                                                       ++ file
                                                       ++ " does not exist and is required for xmonad to work propperly"))

main :: IO Unit
main = do
    home <- getHomeDirectory
    --check background pictures
    mapM_ (\pic -> checkFile $ home ++ background_folder ++ pic) (elems pictures)
    --check xmobar & stalonetray setup files
    checkFile $ (home ++ "/.xmobaruprc")
    checkFile $ (home ++ "/.xmobardownrc")
    checkFile $ (home ++ "/.stalonetrayrc")
    --check ledmon files
    checkFile $ (home ++ "/.xmonad/ledmon")
    checkFile $ (home ++ "/.xmonad/facts.txt")
    --insp_msg
    checkFile $ (home ++ "/.xmonad/insp_msg")
    --scripts to check vol & brightness
    checkFile $ (home ++ "/.xmonad/get-brightness.sh")
    checkFile $ (home ++ "/.xmonad/get-volume.sh")
    
    --Two instances of xmobar, one up
    up <- spawnPipe (home ++ "/.cabal/bin/xmobar ~/.xmobaruprc")
    
    xmonad $ ewmh def
        { manageHook = manageDocks <+> manageHook def
        , layoutHook = avoidStruts $ layoutHook def
        , startupHook = void startup
        , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn up
                        , ppTitle = xmobarColor "green" "" . shorten 50
                        }
        }
        `removeKeys`
        [
          (mod1Mask, xK_w)
        ]
        
        `additionalKeys`
        [
          --Volume Controls
          ((0, xK_F2), void $ toggleMuteChannels ["Master", "Headphone", "Speaker"]),
          ((0, xK_F3), void $ lowerVolume 2.0),
          ((0, xK_F4), void $ raiseVolume 2.0),
          --Brightness Controls
          ((0, xK_F5), spawn "xbacklight - 10%"),
          ((0, xK_F6), spawn "xbacklight + 10%"),
          --Screensaver
          ((mod1Mask .|. shiftMask, xK_z), spawn "xscreensaver-command -lock; xset dpms force off"),
          --commonly used programs bound to keys
          ((mod1Mask, xK_f), spawn "nemo --no-desktop"),
          ((mod1Mask, xK_e), spawn "emacsclient -c"),
          ((mod1Mask, xK_g), spawn "conkeror"),
          ((mod1Mask, xK_m), spawn "urxvt -e cmus"),
          ((mod1Mask .|. controlMask, xK_t), spawn "emacs -q --load ~/.emacsterm --no-splash"),
          ((mod1Mask .|. controlMask, xK_Delete), spawn "gnome-system-monitor"),
          ((mod1Mask .|. controlMask, xK_s), spawn "sudo pm-suspend"),

          
          ((mod1Mask, xK_1), (windows $ greedyView "1") >> (spawn $ "feh --bg-scale " ++ home ++ background_folder ++ (pictures ! 1))),
          ((mod1Mask, xK_2), (windows $ greedyView "2") >> (spawn $ "feh --bg-scale " ++ home ++ background_folder ++ (pictures ! 2))),
          ((mod1Mask, xK_3), (windows $ greedyView "3") >> (spawn $ "feh --bg-scale " ++ home ++ background_folder ++ (pictures ! 3))),
          ((mod1Mask, xK_4), (windows $ greedyView "4") >> (spawn $ "feh --bg-scale " ++ home ++ background_folder ++ (pictures ! 4))),
          ((mod1Mask, xK_5), (windows $ greedyView "5") >> (spawn $ "feh --bg-scale " ++ home ++ background_folder ++ (pictures ! 5))),
          ((mod1Mask, xK_6), (windows $ greedyView "6") >> (spawn $ "feh --bg-scale " ++ home ++ background_folder ++ (pictures ! 6))),
          ((mod1Mask, xK_7), (windows $ greedyView "7") >> (spawn $ "feh --bg-scale " ++ home ++ background_folder ++ (pictures ! 7))),
          ((mod1Mask, xK_8), (windows $ greedyView "8") >> (spawn $ "feh --bg-scale " ++ home ++ background_folder ++ (pictures ! 8))),
          ((mod1Mask, xK_9), (windows $ greedyView "9") >> (spawn $ "feh --bg-scale " ++ home ++ background_folder ++ (pictures ! 9)))
        ]
    return One

startup :: X Unit
startup = do
  home <- liftIO getHomeDirectory
  spawn $ home ++  "/.cabal/bin/xmobar ~/.xmobardownrc"
  spawn "xrdb -merge .Xreasources"
  spawn "stalonetray &"
  spawn "xscreensaver -no-splash &"
  spawn $ "feh --bg-scale " ++ home ++ background_folder ++ (pictures ! 1)
  spawn "udiskie --no-file-manager --automount --notify --tray &"
  spawn "nm-applet --sm-disable &"
  spawn "xfce4-power-manager &"
  spawn "emacs --daemon"
  return One

