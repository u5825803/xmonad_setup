#include <stdio.h>
#include <stdlib.h>

#include "parse.h"

#define NUM_ARGS_MULTIPLY 7

int main(int argc, char * argv[])
{
Argument argumentsMultiply[NUM_ARGS_MULTIPLY] =
{
    NEW_ARG("--width", "-x", "The width, x, of the first, leftmost, input x by y matrix", 1, INT, 8),
    NEW_ARG("--height", "-y", "The height, y, of the first, leftmost, input y by x matrix", 1, INT, 4),
    NEW_ARG("--input-one", "-i1", "The name of the first input file that contains the matrix to be multiplied by the second matrix", 1, STRING, 7),
    NEW_ARG("--input-two", "-i2", "The name of the second input file that contains the matrix to be multiplied by the first matrix", 1, STRING, 3),
    NEW_ARG("--output", "-o", "The name of a file, which the resulting matrix will be written to", 0, STRING, 10),
    NEW_ARG("--parallel", "-p", "Control if program runs in parallel", 1, BOOL, 1),
    NEW_ARG("--help", "-h", "Display help message", 0, BOOL, 3)
};
    PARSE_ERROR i = parse(argumentsMultiply, NUM_ARGS_MULTIPLY, argv, argc);
    if (i.code == PARSE_SUCCESS){
        int n;
        for (n = 0; n < NUM_ARGS_MULTIPLY; n++){
            printf("longcode: %s, shortcode: %s, description: %s, value: %d\n", argumentsMultiply[n].longcode, argumentsMultiply[n].shortcode, argumentsMultiply[n].description, argumentsMultiply[n].value.i);
        }
        printf("Arg value was: %d", argumentsMultiply[0].value.i);
    }
    else
        printf("Parser exited with error code: %d on flag: %s", i.code, i.flag);

    return 0;
}
