/*
Created by Andrew M. Hall
*/

#include <stdlib.h>
#include <string.h>

#include "parse.h"

#define TRUE 1
#define FALSE 0


PARSE_ERROR createError(const char* flag, PARSE_ERROR_CODE code){
    PARSE_ERROR err = {.code=code, .flag=flag};
    return err;
}

PARSE_ERROR parse(Argument* argList, int numArgs, char* args[], int argc){
    int given[1024] = {FALSE};

    int i = 1; //First argument will be Program name
    int n;
    while (i < argc){
        for (n = 0; n < numArgs; n++){
            if(!strcmp(args[i], argList[n].shortcode) || !strcmp(args[i], argList[n].longcode)){
                int tmpI; float tmpF;
                switch (argList[n].type){
                case INT:
                    tmpI = atoi(args[i+1]);
                    if (tmpI == 0)
                        return createError(args[i+1], INT_CAST_ERROR);
                    argList[n].value.i = tmpI;
                    i += 2;
                    break;
                case FLOAT:
                    tmpF = atof(args[i+1]);
                    if (tmpF == 0)
                        return createError(args[i+1], FLOAT_CAST_ERROR);
                    argList[n].value.f = (float)tmpF;
                    i+=2;
                    break;
                case STRING:
                    strncpy(argList[n].value.s, args[i+1], CODE_LENGTH);
                    i += 2;
                    break;
                case BOOL:
                    argList[n].value.b = TRUE;
                    i++;
                    break;
                default:
                    return createError(args[i], PARSE_FAILURE);
                }

                if (argList[n].mandatory)
                    given[n] = TRUE;
                break;
            }
            if (n == numArgs-1)
                return createError(args[i],INVALID_FLAG);
        }
    }

    //Check mandatory arguments given
    for (n = 0; n < numArgs && n < 1024; n++){
        if (argList[n].mandatory && !given[n])
            return createError(argList[n].longcode, MANDATORY_ARG_MISSING);
    }

    return createError(NULL, PARSE_SUCCESS);
}
