#ifndef PARSE_H_INCLUDED
#define PARSE_H_INCLUDED

#define NEW_ARG(longCode, shortCode, arg_description, is_mandatory, datatype, arg_code) {.longcode=longCode, .shortcode=shortCode, .description=arg_description, .mandatory=is_mandatory, .type=datatype, .code=arg_code, .value.i=0}
#define CODE_LENGTH 64
#define DESCRIPTION_LENGH 1024

typedef enum {
    INT = 0,
    FLOAT,
    STRING,
    BOOL
} TYPE;

typedef union {
    int i;
    float f;
    char s[CODE_LENGTH];
    int b;
} Value;

typedef struct {
    const char longcode[CODE_LENGTH];
    const char shortcode[CODE_LENGTH];
    const char description[DESCRIPTION_LENGH];
    int mandatory;
    TYPE type;
    int code;
    Value value;
} Argument;

typedef enum {
    INT_CAST_ERROR = 0,
    FLOAT_CAST_ERROR,
    STRING_CPY_ERROR,
    MANDATORY_ARG_MISSING,
    INVALID_FLAG,
    PARSE_FAILURE,
    PARSE_SUCCESS
} PARSE_ERROR_CODE;

typedef struct {
    PARSE_ERROR_CODE code;
    const char* flag;
} PARSE_ERROR;

PARSE_ERROR parse(Argument* argList, int numArgs, char* args[], int argc);

#endif // PARSE_H_INCLUDED
